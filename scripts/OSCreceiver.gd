extends OSCreceiver

signal create(name)  # str
signal aeg()  # str, array

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("osc_message_received", self, "_on_msg")
	set_process(true)


func _on_msg(msg):
	if (msg.empty()):
		return
	if msg.address() == "/viz/create":
		_create_sound_source(msg)
	if msg.address() == "/viz/aeg":
		_handle_aeg(msg)

func _create_sound_source(msg:OSCmessage):
	emit_signal("create", msg.arg(0))	
	
func _handle_aeg(msg:OSCmessage):
	var name = msg.arg(0)
	var a = msg.arg(1)
	var e = msg.arg(2)
	var g = msg.arg(3)
	emit_signal("aeg", name, a, e, g)

extends Spatial

onready var osc = $OSCreceiver

var sound_source = preload("res://scenes/SoundSource.tscn")

var instances = []

# Called when the node enters the scene tree for the first time.
func _ready():
	osc.connect("create", self, "_on_create")
	osc.connect("aeg", self, "_on_aeg")
	
func _on_create(name):
	print("_on_create: " + name)
	var instance = sound_source.instance()
	instance.name = name
	instances.append(instance)
	add_child(instance)

func aeg2xyz(aeg: Array):
	var azi = deg2rad(aeg[0])
	var ele = deg2rad(aeg[1])
	var r = gain2radius(aeg[2])
	var x = r * cos(ele) * sin(azi)
	var y = r * sin(ele) * cos(azi)
	var z = r * cos(azi)
	return [x,y,z]
	
func gain2radius(gain: float):
	return log(pow(10, abs(gain)))/20
	
func _on_aeg(name:String, a:float, e:float, g:float):
	var xyz = aeg2xyz([a,e,g])
	for i in instances:
		if i.name == name:
			var s = i
			i.translation = Vector3(xyz[0], xyz[2], xyz[1])
			print(name + ": ", aeg2xyz([a,e,g]))
		else: 
			print("don't see: ", name)
